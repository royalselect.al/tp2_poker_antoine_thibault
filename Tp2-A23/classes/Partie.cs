﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace Tp2_A23.classes
{
    public class Partie
    {
        public static int smallblind = 1;
        public static int bigblind = 2;

        //TODO #1
        public Queue<Joueur> Joueurs { get; set; }
        public Stack<Carte> Paquet { get; set; }
        public Stack<Carte> Defausse { get; set; }
        public List<Carte> CartesCommunes { get; set; }
        public int MiseMinimale { get; set; }
        public int IndexDonneur { get; set; }
        private Joueur Donneur { get; set; }

        public Joueur? DernierRelanceur { get; set; }

        public Partie(Utilisateur pUtilisateur)
        {
            Joueurs = GenererJoueur(pUtilisateur);
            Paquet = CreerJeuCartes();
            Paquet = BrasserJeuCartes(Paquet);
            Defausse = new Stack<Carte>();
            CartesCommunes = new List<Carte>();
            IndexDonneur = -1;
            Donneur = Joueurs.Peek();
        }

        #region "Action joueur humain"
        /// <summary>
        /// Permet de faire relancer le joueur humain.
        /// </summary>
        /// <returns>L'action jouée</returns>
        public string JoueurCourantRelance()
        {
            
            string messageRetour = Joueurs.Peek().Relancer(MiseMinimale, bigblind);
            MiseMinimale = Joueurs.Peek().Mise;
            DernierRelanceur = Joueurs.Peek();
            Joueurs.Enqueue(Joueurs.Dequeue());
            return messageRetour;
        }

        /// <summary>
        /// Permet de faire suivre le joueur humain.
        /// </summary>
        /// <returns>L'action jouée</returns>
        public string JoueurCourantSuit()
        {
            string messageRetour = Joueurs.Peek().Suivre(MiseMinimale);
            Joueurs.Enqueue(Joueurs.Dequeue());
            return messageRetour;
        }

        /// <summary>
        /// Permet de faire parler le joueur humain.
        /// </summary>
        /// <returns>L'action jouée</returns>
        public string JoueurCourantParle()
        {
            string messageRetour = Joueurs.Peek().Parler();
            Joueurs.Enqueue(Joueurs.Dequeue());
            return messageRetour;
        }

        /// <summary>
        /// Permet de faire coucher le joueur humain.
        /// </summary>
        /// <returns>L'action jouée</returns>
        public string JoueurCourantSeCouche()
        {
            string messageRetour = Joueurs.Peek().SeCoucher();
            Joueurs.Enqueue(Joueurs.Dequeue());
            return messageRetour;
        }
        #endregion

        #region "Gestion d'un tour"
        /// <summary>
        /// Cette méthode permet de commencer une main
        /// </summary>
        public void CommencerUneMain()
        {
            DistribuerCartes();

            while(Joueurs.Peek() != Donneur)
                Joueurs.Enqueue(Joueurs.Dequeue());

            Joueurs.Enqueue(Joueurs.Dequeue());
            Donneur = Joueurs.Peek();

            Joueurs.Peek().Miser(smallblind);
            Joueurs.Enqueue(Joueurs.Dequeue());

            Joueurs.Peek().Miser(bigblind);
            DernierRelanceur = Joueurs.Peek();
            Joueurs.Enqueue(Joueurs.Dequeue());

            IndexDonneur = (IndexDonneur + 1) % 4;
            
            
            MiseMinimale = bigblind;
        }

        /// <summary>
        /// Cette méthode permet de faire jouer le prochain joueur. Elle retourne une 
        /// string qui indique le coup joué, sauf pour ces cas d'exception :
        /// 
        /// 1. Si c'est au tour du joueur humain, elle retourne une string pour l'annoncer. 
        /// 2. Si c'est la fin du tour, elle retourne une string pour l'annoncer.
        /// 3. Si le joueur est couché, elle retourne une string pour l'annoncer.
        /// </summary>
        /// <returns>Le coup joué ou un message</returns>
        public string JouerProchainJoueur()
        {
            string messageRetour;

            if (Joueurs.Peek() == DernierRelanceur)
                return "FinTour";

            if (DernierRelanceur == null)
                DernierRelanceur = Joueurs.Peek();

            if (Joueurs.Peek() is JoueurAutomatise)
            {
                if (Joueurs.Peek().EtatJoueur == Joueur.Etat.EnJeu)
                {
                    messageRetour = ((JoueurAutomatise)Joueurs.Peek()).Jouer(MiseMinimale, bigblind, CartesCommunes);
                    if (Joueurs.Peek().Mise > MiseMinimale)
                    {
                        MiseMinimale = Joueurs.Peek().Mise;
                        DernierRelanceur = Joueurs.Peek();
                    }
                }
                else
                    messageRetour = "JoueurCouche";
            }
            else
            {
                if (Joueurs.Peek().EtatJoueur == Joueur.Etat.EnJeu)
                    return  "AttenteJoueurHumain";
                else
                    messageRetour = "JoueurCouche";
            }
            Joueurs.Enqueue(Joueurs.Dequeue());
            return messageRetour;
        }

        /// <summary>
        /// Cette methode permet de vider la table à la fin d'une main. Afin de préparer
        /// la main suivante, si le paquet ne contient pas 13 cartes, nous brassons la 
        /// défausse et le paquet ensemble et remettons les 52 cartes brassées dans le paquet.
        /// </summary>
        public void ViderTable()
        {
            foreach (Joueur joueur in Joueurs)
            {
                joueur.EtatJoueur = Joueur.Etat.EnJeu;
                joueur.Mise = 0;
                foreach(Carte carte in joueur.Main)
                {
                    Defausse.Push(carte);
                }
                joueur.Main.Clear();
            }
            foreach (Carte carte in CartesCommunes)
            {
                Defausse.Push(carte);
            }
            CartesCommunes.Clear();

            if (Paquet.Count < 13)
            {
                while (Paquet.Count > 0)
                {
                    Defausse.Push(Paquet.Pop());
                }
                Paquet = BrasserJeuCartes(Defausse);
            }
        }
        #endregion

        #region "Générations"
        /// <summary>
        /// Cette méthode initialise la liste de joueurs.
        /// </summary>
        private Queue<Joueur> GenererJoueur(Utilisateur pUtilisateur)
        {
            Queue<Joueur> lesJoueurs = new Queue<Joueur>();
            lesJoueurs.Enqueue(new Joueur(pUtilisateur));
            lesJoueurs.Enqueue(new JoueurAutomatise(new Utilisateur("J2")));
            lesJoueurs.Enqueue(new JoueurAutomatise(new Utilisateur("J3")));
            lesJoueurs.Enqueue(new JoueurAutomatise(new Utilisateur("J4")));
            return lesJoueurs;
        }


        /// <summary>
        /// Cette méthode crée un jeu de 52 cartes sans les jokers.
        /// </summary>
        private Stack<Carte> CreerJeuCartes()
        {
            Stack<Carte> nouveauPaquet = new Stack<Carte>();
            foreach (Carte.Sorte laSorte in Enum.GetValues(typeof(Carte.Sorte)))
            {
                for (int i = 2; i < 15; i++)
                {
                    nouveauPaquet.Push(new Carte(i, laSorte));
                }
            }
            return nouveauPaquet;
        }
        #endregion

        #region "Distribution des cartes"
        /// <summary>
        /// Cette méthode distribue les cartes pour le départ d'une partie.
        /// On distribue 2 cartes à tous les joueurs et 3 cartes communes.
        /// </summary>
        private void DistribuerCartes()
        {

            for (int i = 0; i < 2; i++)
            {
                foreach (Joueur joueur in Joueurs)
                {
                    joueur.Main.Add(Paquet.Pop());
                    
                }
            }

            for (int i = 0;i < 3;i++)
            {
                CartesCommunes.Add(Paquet.Pop());
            }
        }

        /// <summary>
        /// Cette méthode permet d'ajouter les cartes communes 4 et 5. Aussi connue
        /// sous le nom de tournant et rivière.
        /// </summary>
        public void TournantEtRiviere()
        {
            CartesCommunes.Add(Paquet.Pop());
            DernierRelanceur = null;
        }

        /// <summary>
        /// Cette méthode permet de brasser une liste de carte. Elle
        /// vide la liste passée en paramètre et retourne une nouvelle
        /// liste brassée
        /// </summary>
        /// <param name="pileABrasser">La liste à brasser</param>
        /// <returns>Une nouvelle liste brassée</returns>
        private Stack<Carte> BrasserJeuCartes(Stack<Carte> pileABrasser)
        {
            List<Carte> listeBrasse = new List<Carte>();
            while (pileABrasser.Count > 0)
            {
                listeBrasse.Add(pileABrasser.Pop());
            }

            int i;
            while (listeBrasse.Count > 0)
            {
                i = Utilitaires.rnd.Next(listeBrasse.Count);
                pileABrasser.Push(listeBrasse[i]);
                listeBrasse.RemoveAt(i);
            }

            return pileABrasser;
        }
        #endregion

        //TODO #4
        /// <summary>
        /// Cette méthode est appelée à la fin d'une main.
        /// Elle trouve le gagnant selon ce classement : 
        /// https://www.poker.org/poker-hands-ranking-chart/
        /// 
        /// Elle ajuste ensuite les gains du joueur humain ainsi
        /// que le classement en conséquence.
        /// </summary>
        /// <returns>Le nom du gagnant ainsi que le montant</returns>
        public string FinDeMain()
        {
            Joueur gagnant = Joueurs.Peek();
            Joueur joueurHumain = Joueurs.Peek();
            joueurHumain.Utilisateur.MainJouees[joueurHumain.Utilisateur.MainJouees.Count - 1].cartes
                = joueurHumain.Main[0].ToString() + " " + joueurHumain.Main[1].ToString();
            int valeurMax = 0;
            int valeurActuelle = 0;

            int gainsHumain = 0;

            for (int i = 0; i < 4; i++)
            {
                if (Joueurs.Peek().EtatJoueur == Joueur.Etat.Couche)
                    valeurActuelle = -1;
                else
                {
                    List<Carte> cartes = Joueurs.Peek().Main.Concat(CartesCommunes).ToList();
                    ResultatFinMain resultatFinMain = new ResultatFinMain(cartes);
                    valeurActuelle = resultatFinMain.ObtenirValeurMain();
                }

                if (valeurActuelle > valeurMax)
                {
                    gagnant = Joueurs.Peek();
                    valeurMax = valeurActuelle;
                }

                if (!(Joueurs.Peek() is JoueurAutomatise))
                    joueurHumain = Joueurs.Peek();
                else
                    gainsHumain += Joueurs.Peek().Mise;

                Joueurs.Enqueue(Joueurs.Dequeue());
            }

            if (gagnant == joueurHumain)
            {
                joueurHumain.Utilisateur.Gains += gainsHumain;
                joueurHumain.Utilisateur.MainJouees[joueurHumain.Utilisateur.MainJouees.Count - 1].gains = gainsHumain;
                joueurHumain.Utilisateur.MainJouees[joueurHumain.Utilisateur.MainJouees.Count - 1].estGagne = true;
            }
            else
            {
                joueurHumain.Utilisateur.Gains -= joueurHumain.Mise;
                joueurHumain.Utilisateur.MainJouees[joueurHumain.Utilisateur.MainJouees.Count - 1].gains = -joueurHumain.Mise;
            }
            
            joueurHumain.Utilisateur.MainJouees[joueurHumain.Utilisateur.MainJouees.Count - 1].miseFinale = joueurHumain.Mise; 
            
            Donnees.MettreAJourClassement(joueurHumain.Utilisateur);
            Donnees.TrierClassement(joueurHumain.Utilisateur);
            Donnees.MettreAJourStats(joueurHumain.Utilisateur);
            Donnees.SauvegarderDonnees();

            return $"{gagnant.Utilisateur.Nom} gagne avec une mise de {gagnant.Mise}";
        }

        public void RemettreQueueJoueurEtatInitial()
        {
            while (Joueurs.Peek() is JoueurAutomatise)
            {
                Joueurs.Enqueue(Joueurs.Dequeue());
            }
        }
    }
}
