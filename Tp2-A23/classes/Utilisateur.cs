﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Tp2_A23.classes
{
    [Serializable]
    public class Utilisateur
    {
        public string Nom {  get; set; }
        public byte[] Mdp { get; set; }
        public int Gains { get; set; }

        public List<mainJouee> MainJouees = new List<mainJouee>();  

        public Utilisateur() { }
        public Utilisateur(string pNom)
        {
            Nom= pNom;
        }

        public Utilisateur(string pNom, byte[] pMdp, int pGain = 0) : this(pNom)
        {
            Mdp = pMdp;
            Gains = pGain;
        }

        public override bool Equals(object? obj)
        {
            if (obj == null) 
                return false;
            if (!(obj is Utilisateur)) 
                return false;

            return Nom == ((Utilisateur)obj).Nom;
        }

    }
}
