﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tp2_A23.classes
{
    public class Joueur
    {

        public enum Etat
        {
            Couche,
            Parle,
            EnJeu
        }
        public Etat EtatJoueur { get; set; }


        public Utilisateur Utilisateur { get; set; }
        public List<Carte> Main { get; set; }
        public int Mise {  get; set; }
        public int Fonds { get; set; }

        

        public Joueur(Utilisateur pUtilisateur) 
        { 
            Utilisateur = pUtilisateur;
            Main = new List<Carte>(2);
            EtatJoueur = Etat.EnJeu;
            Fonds = 50;
            //Utilisateur.MainJouees[Main.Count - 1].cartes = Main;
        }

        
        #region "Actions possibles"
        /// <summary>
        /// Permet au joueur de miser
        /// </summary>
        /// <param name="pMiseAjout">Le montant à ajouter dans la mise du joueur</param>
        public void Miser(int pMiseAjout)
        {
            Mise += pMiseAjout;
            Fonds -= pMiseAjout;
        }

        /// <summary>
        /// Permet au joueur de se coucher
        /// </summary>
        /// <returns>Une string expliquant l'action</returns>
        public string SeCoucher()
        {

            EtatJoueur = Etat.Couche;
            if (Utilisateur.MainJouees.Count > 0 )
            {
                Utilisateur.MainJouees[Utilisateur.MainJouees.Count - 1].nbCouche++;
            }
            return Utilisateur.Nom + " " + "s'est couché.";

        }

        /// <summary>
        /// Permet au joueur de parler
        /// </summary>
        /// <returns>Une string expliquant l'action</returns>
        public string Parler()
        {
            EtatJoueur = Etat.Parle;
            if (Utilisateur.MainJouees.Count > 0)
            {
                Utilisateur.MainJouees[Utilisateur.MainJouees.Count - 1].nbParle++;
            }
            return Utilisateur.Nom + " " + "a parlé.";
        }

        /// <summary>
        /// Permet au joueur de suivre
        /// </summary>
        /// <param name="pMiseMinimale"></param>
        /// <returns>Une string expliquant l'action</returns>
        public string Suivre(int pMiseMinimale)
        {
            Miser(pMiseMinimale - Mise);
            if (Utilisateur.MainJouees.Count > 0)
            {
                Utilisateur.MainJouees[Utilisateur.MainJouees.Count -1].nbSuivi++;
            }
            return Utilisateur.Nom + " " + "a suivi.";
        }

        /// <summary>
        /// Permet au joueur de relancer
        /// </summary>
        /// <param name="pMiseMinimale">La plus haute mise en jeu</param>
        /// <param name="pBigblind">Le montant pour relancer la mise</param>
        /// <returns>Une string expliquant l'action</returns>
        public string Relancer(int pMiseMinimale, int pBigblind)
        {
            Miser(pMiseMinimale - Mise + pBigblind);
            if (Utilisateur.MainJouees.Count > 0)
            {
                Utilisateur.MainJouees[Utilisateur.MainJouees.Count - 1].nbRelance++;
            }
            return Utilisateur.Nom + " " + "a relancé.";
        }
        #endregion

        #region "Override pour comparaisons"
        /// <summary>
        /// Override du equals pour la comparaison soit faîte avec le nom.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object? obj)
        {
            if (obj != null)    
                return Utilisateur.Nom == ((Joueur)obj).Utilisateur.Nom;
            else
                return false;
        }

        public override int GetHashCode()
        {
            return Utilisateur.Nom.GetHashCode();
        }
        #endregion
    }
}
