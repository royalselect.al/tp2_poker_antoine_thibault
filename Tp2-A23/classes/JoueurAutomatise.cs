﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tp2_A23.classes
{
    public class JoueurAutomatise : Joueur
    {
        public JoueurAutomatise(Utilisateur pUtilisateur) : base(pUtilisateur)
        { }

        /// <summary>
        /// Algo décisionnel pour faire jouer un joueur automatisé
        /// </summary>
        /// <param name="pMiseMinimale">La plus haute mise en jeu</param>
        /// <param name="pBigblind">Le montant pour relancer</param>
        /// <returns></returns>
        public string Jouer(int pMiseMinimale, int pBigblind, List<Carte> pCartesCommunes)
        {
            //TODO #7
            ResultatFinMain resultatMain = new ResultatFinMain(Main.Concat(pCartesCommunes).ToList());
            int valeurMain = resultatMain.ObtenirValeurMain();

            if (valeurMain >= ResultatFinMain.STRAIGHT)
                return Relancer(pMiseMinimale, pBigblind);

            else
            {
                int choix = Utilitaires.rnd.Next(101) + 1;
                if (valeurMain >= ResultatFinMain.THREE_OF_A_KIND)
                {
                    if (choix <= 75)
                        return Relancer(pMiseMinimale, pBigblind);
                    else
                        return Suivre(pMiseMinimale);
                }
                else
                {
                    if (((double)Mise / Fonds) * 100 > 10)
                    {
                        if (choix <= 70)
                            return Parler();
                        else if (choix > 70 && choix <= 90)
                            return Suivre(pMiseMinimale);

                        return Relancer(pMiseMinimale, pBigblind);
                    }
                    else
                    {
                        if (choix <= 50)
                            return Parler();
                        else if (choix > 50 && choix <= 80)
                            return Suivre(pMiseMinimale);

                        return SeCoucher();
                    }
                }
            }
        }
    }
}
