﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tp2_A23.classes
{
    public class ResultatFinMain
    {
        #region Attributs
        List<Carte> cartes = new List<Carte>();

        int suiteEnCours;
        int suiteMax;
        Carte carteSuite;

        int suiteMemeSorteEnCours;
        int suiteMemeSorteMax;
        Carte carteSuiteMemeSorte;

        Dictionary<Carte.Sorte, int> nbCartesParSorte = new Dictionary<Carte.Sorte, int>();
        Carte carteFlush;

        int nbMemeCarte;
        LinkedList<Carte> pairs = new LinkedList<Carte>();
        Carte carteTriple;
        Carte carteQuadruple;

        public static int ROYAL_FLUSH = 900;
        public static int STRAIGHT_FULL = 800;
        public static int FOUR_OF_A_KIND = 700;
        public static int FULL_HOUSE = 600;
        public static int FLUSH_VALUE = 500;
        public static int STRAIGHT = 400;
        public static int THREE_OF_A_KIND = 300;
        public static int TWO_PAIR = 200;
        public static int PAIR = 100;

        #endregion

        #region Constructeurs
        public ResultatFinMain(List<Carte> pCartes)
        {
            cartes = pCartes;
            cartes.Sort(Carte.TriValeurAscendant);

            suiteEnCours = 1;
            suiteMax = 0;
            carteSuite = null;

            suiteMemeSorteEnCours = 1;
            suiteMemeSorteMax = 0;
            carteSuiteMemeSorte = null;

            nbCartesParSorte.Add(Carte.Sorte.Coeur, 0);
            nbCartesParSorte.Add(Carte.Sorte.Pique, 0);
            nbCartesParSorte.Add(Carte.Sorte.Carreau, 0);
            nbCartesParSorte.Add(Carte.Sorte.Trèfle, 0);
            carteFlush = null;

            nbMemeCarte = 1;
            carteTriple = null;
            carteQuadruple = null;
        }

        #endregion

        #region Methodes
        /// <summary>
        /// Trouve les suites
        /// </summary>
        private void TrouverSuites()
        {
            int i = 1;
            do
            {
                if (cartes[i].Valeur == cartes[i - 1].Valeur + 1)
                {
                    suiteEnCours++;
                    nbMemeCarte = 1;

                    if (suiteEnCours >= suiteMax)
                    {
                        suiteMax = suiteEnCours;
                        carteSuite = cartes[i];
                    }

                    if (cartes[i].SorteCarte == cartes[i - 1].SorteCarte)
                    {
                        suiteMemeSorteEnCours++;
                        if (suiteMemeSorteEnCours >= suiteMemeSorteMax)
                        {
                            suiteMemeSorteMax = suiteMemeSorteEnCours;
                            carteSuiteMemeSorte = cartes[i];
                        }
                    }
                    else
                        suiteMemeSorteEnCours = 1;
                }
                else if (cartes[i].Valeur != cartes[i - 1].Valeur)
                {
                    suiteEnCours = 1;
                    suiteMemeSorteEnCours = 1;
                }

                if (i > 2 && suiteEnCours < 2)
                    return;

                i++;
            } while (i < cartes.Count);
        }

        /// <summary>
        /// Trouve les pairs, triple et quadruple
        /// </summary>
        private void TrouverDoublons()
        {
            int i = 1;
            do
            {
                if (cartes[i].Valeur == cartes[i - 1].Valeur)
                {
                    nbMemeCarte++;
                    switch (nbMemeCarte)
                    {
                        case 2:
                            pairs.AddLast(cartes[i]);
                            break;
                        case 3:
                            pairs.Remove(Utilitaires.TrouverNoeud(pairs, cartes[i]));
                            carteTriple = cartes[i];
                            break;
                        case 4:
                            pairs.Clear();
                            carteTriple = null;
                            carteQuadruple = cartes[i];
                            return;
                    }
                }
                else
                    nbMemeCarte = 1;

                i++;
            } while (i < cartes.Count);
        }

        /// <summary>
        /// Trouve toutes les combinaisons possible avec la main du joueur et les cartes communes
        /// </summary>
        private void CalculerStatsMain()
        {
            TrouverSuites();
            if (suiteMemeSorteMax >= 5)
                return;
            TrouverDoublons();
            if (carteQuadruple != null || carteTriple != null && pairs.Count > 0)
                return;

            for (int j = 1; j < cartes.Count; j++)
            {
                nbCartesParSorte[cartes[j].SorteCarte]++;
                if (nbCartesParSorte[cartes[j].SorteCarte] >= 5)
                    carteFlush = cartes[j];
            }
        }

        /// <summary>
        /// Calcule les combinaisons de la main du joueurs et des cartes communes et 
        /// renvoit la valeur de la combinaison la plus forte
        /// </summary>
        /// <returns>La valeur de la combinaison la plus forte</returns>
        public int ObtenirValeurMain()
        {
            CalculerStatsMain();

            if (suiteMemeSorteMax >= 5)
            {
                if (carteSuiteMemeSorte.Valeur == 14)
                    return ROYAL_FLUSH;

                return STRAIGHT_FULL + carteSuiteMemeSorte.Valeur;
            }
            if (carteQuadruple != null)
                return FOUR_OF_A_KIND + carteQuadruple.Valeur;

            if (carteTriple != null && pairs.Count > 0)
                return FULL_HOUSE + carteTriple.Valeur + pairs.First.Value.Valeur;

            if (carteFlush != null)
                return FLUSH_VALUE + carteFlush.Valeur;

            if (suiteMax >= 5)
                return STRAIGHT + carteSuite.Valeur;

            if (carteTriple != null)
                return THREE_OF_A_KIND + carteTriple.Valeur;

            if (pairs.Count >= 2)
                return TWO_PAIR + pairs.Last.Value.Valeur + pairs.Last.Previous.Value.Valeur;

            if (pairs.Count == 1)
                return PAIR + pairs.First.Value.Valeur;

            return cartes[cartes.Count - 1].Valeur;
        }

        #endregion
    }
}
