﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tp2_A23.classes
{
    public class Carte
    {
        public enum Sorte
        {
            Coeur,
            Carreau,
            Trèfle,
            Pique
        }

        public Sorte SorteCarte { get; set; }
        public int Valeur { get; set; }

        public Carte(int pValeur, Sorte pSorte)
        {
            Valeur = pValeur;
            SorteCarte = pSorte;
        }

        /// <summary>
        /// Comparaison entre 2 cartes pour les trier en ordre croissant
        /// </summary>
        /// <param name="pCarte1">Carte 1</param>
        /// <param name="pCarte2">Carte 2</param>
        /// <returns>
        /// -1 si la valeur de la carte 1 est inférieure à celle de la carte 2.
        /// 0 si les valeurs des deux cartes sont identiques.
        /// 1 si la valeur de la carte 1 est supérieure à celle de la carte 2.
        /// </returns>
        public static int TriValeurAscendant(Carte pCarte1, Carte pCarte2)
        {
            return pCarte1.Valeur.CompareTo(pCarte2.Valeur);
        }

        public override bool Equals(object? obj)
        {
            if (obj == null) 
                return false;
            if (!(obj is Carte)) 
                return false;
            return Valeur == ((Carte)obj).Valeur && SorteCarte == ((Carte)obj).SorteCarte;
        }

        #region NomFichierPourAffichage

        /// <summary>
        /// Cette méthode permet d'obtenir le nom du fichier .png
        /// afin d'afficher la carte.
        /// </summary>
        /// <returns>Le nom du fichier .png</returns>
        public string ObtenirNomFichier()
        {
            return ObtenirChaineValeur() + ObtenirChaineSorte() + ".png";
        }

        /// <summary>
        /// Cette méthode récris le toString() d'une carte
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return ObtenirChaineValeur() + ObtenirChaineSorte();
        }

        /// <summary>
        /// Cette méthode permet d'obtenir la chaine contenant la
        /// valeur de la carte.
        /// </summary>
        /// <returns>Une string contenant la valeur</returns>
        private string ObtenirChaineValeur()
        {
            switch (Valeur)
            {
                case < 11:
                    return Valeur.ToString();
                case 11:
                    return "V";
                case 12:
                    return "D";
                case 13:
                    return "R";
                case 14:
                    return "A";
                default:
                    return "";
            }
        }


        /// <summary>
        /// Cette méthode permet d'obtenir la chaine contenant la
        /// sorte de la carte.
        /// </summary>
        /// <returns>Une string contenant la sorte</returns>
        private string ObtenirChaineSorte()
        {
            switch (SorteCarte)
            {
                case Sorte.Carreau:
                    return "CA";
                case Sorte.Coeur:
                    return "CO";
                case Sorte.Pique:
                    return "PI";
                case Sorte.Trèfle:
                    return "TR";
                default:
                    return "";
            }
        }
        #endregion
    }
}
