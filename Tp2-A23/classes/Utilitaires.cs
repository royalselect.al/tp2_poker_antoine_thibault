﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tp2_A23.classes
{
    public static class Utilitaires
    {
        public static Random rnd = new Random();

        /// <summary>
        /// Trouve un noeud dans une liste liée
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="pListeLiee">La liste liée dans laquelle chercher un noeud</param>
        /// <param name="pValeurATrouver">La valeur du noeud à trouver</param>
        /// <returns></returns>
        public static LinkedListNode<T> TrouverNoeud<T>(LinkedList<T> pListeLiee, T pValeurATrouver)
        {
            LinkedListNode<T> noeudATrouver = pListeLiee.First;

            while (noeudATrouver.Next != null && !noeudATrouver.Value.Equals(pValeurATrouver))
            {
                noeudATrouver = noeudATrouver.Next;
            }

            return noeudATrouver;
        }
    }
}
