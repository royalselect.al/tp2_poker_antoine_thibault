﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Tp2_A23.classes;

namespace Tp2_A23
{
    /// <summary>
    /// Logique d'interaction pour Statistiques.xaml
    /// </summary>
    public partial class Statistiques : Window
    {
        public Statistiques(Utilisateur pUtilisateur)
        {
            InitializeComponent();
            txtMains.Text = pUtilisateur.MainJouees.Count.ToString();
            txtGains.Text = pUtilisateur.Gains.ToString();
            int nbVictoire = 0;
            foreach(mainJouee main in pUtilisateur.MainJouees)
            {
                if(main.estGagne)
                {
                    nbVictoire++;
                }
            }
            txtVictoires.Text = nbVictoire.ToString();
            List<mainJouee> listAffichage = pUtilisateur.MainJouees;
            listAffichage.Reverse();
            dgStatistiques.ItemsSource = listAffichage;
        }


    }
}
