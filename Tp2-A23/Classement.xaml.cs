﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Tp2_A23.classes;

namespace Tp2_A23
{
    /// <summary>
    /// Logique d'interaction pour Classement.xaml
    /// </summary>
    public partial class Classement : Window
    {
        //TODO #2
        //Retirer la liste en param.
        public Classement()
        {
            InitializeComponent();
            dgClassement.ItemsSource = Donnees.classement;
        }

        private void dgClassement_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();
        }
    }
}
