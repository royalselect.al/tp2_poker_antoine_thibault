﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tp2_A23.classes;

namespace Tp2_A23
{
    public class mainJouee
    {
        public string cartes { get; set; } 
        public int miseFinale { get; set; }
        public bool estGagne { get; set; }
        public int gains { get; set; }
        public int nbRelance { get; set; }
        public int nbSuivi { get; set; }
        public int nbParle { get; set; }
        public int nbCouche { get; set; }

        
        public mainJouee()
        {

        }
    }
}
