﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tp2_A23.classes;

namespace Tp2_A23
{
    public static class Donnees
    {
        public static Dictionary<string, Utilisateur> dictUtilisateurs = new Dictionary<string, Utilisateur>();
        public static Dictionary<string, byte[]> dictSalts = new Dictionary<string, byte[]>();
        public static LinkedList<Utilisateur> classement = new LinkedList<Utilisateur>();

        /// <summary>
        /// Charge les données des utilisateurs, de leur sels et du classement des utilisateurs
        /// </summary>
        public static void ChargerDonnees()
        {
            if (File.Exists("utilisateurs.data") && File.Exists("salt.data") && File.Exists("classement.data"))
            {
                using (StreamReader sr = new StreamReader("utilisateurs.data"))
                    dictUtilisateurs = JsonConvert.DeserializeObject<Dictionary<string, Utilisateur>>(sr.ReadToEnd());

                using (StreamReader sr = new StreamReader("salt.data"))
                    dictSalts = JsonConvert.DeserializeObject<Dictionary<string, byte[]>>(sr.ReadToEnd());

                using (StreamReader sr = new StreamReader("classement.data"))
                    classement = JsonConvert.DeserializeObject<LinkedList<Utilisateur>>(sr.ReadToEnd());

            }
        }

        /// <summary>
        /// Sauvegarde les données des utilisateurs, de leur sels et du classement des utilisateurs
        /// </summary>
        public static void SauvegarderDonnees()
        {
            using (StreamWriter sw = new StreamWriter("utilisateurs.data"))
                sw.Write(JsonConvert.SerializeObject(dictUtilisateurs));
            
            using (StreamWriter sw = new StreamWriter("salt.data"))
                sw.Write(JsonConvert.SerializeObject(dictSalts));

            using (StreamWriter sw = new StreamWriter("classement.data"))
                sw.Write(JsonConvert.SerializeObject(classement));

        }

        /// <summary>
        /// Met à jour la valeur des gains d'un utilisateur dans le classement
        /// </summary>
        /// <param name="pUtilisateur">L'utilisateur dont les gains sont actualisés</param>
        public static void MettreAJourClassement(Utilisateur pUtilisateur)
        {
            Utilitaires.TrouverNoeud(classement, pUtilisateur).Value.Gains = pUtilisateur.Gains;
        }

        /// <summary>
        /// Trie le classement des utilisateurs en ordre décroissant en fonction de leurs gains
        /// </summary>
        /// <param name="pUtilisateur">L'utilisateur qui est authentifié au moment du tri</param>
        public static void TrierClassement(Utilisateur pUtilisateur)
        {
            LinkedListNode<Utilisateur> noeudUtilisateur = Utilitaires.TrouverNoeud(classement, pUtilisateur);

            while (noeudUtilisateur.Next != null
            && noeudUtilisateur.Next.Value.Gains > pUtilisateur.Gains)
            {
                LinkedListNode<Utilisateur> noeudTemp = noeudUtilisateur.Next;
                classement.Remove(noeudUtilisateur);
                classement.AddAfter(noeudTemp, pUtilisateur);
                noeudUtilisateur = noeudTemp.Next;
            }
            while (noeudUtilisateur.Previous != null
            && noeudUtilisateur.Previous.Value.Gains < pUtilisateur.Gains)
            {
                LinkedListNode<Utilisateur> noeudTemp = noeudUtilisateur.Previous;
                classement.Remove(noeudUtilisateur);
                classement.AddBefore(noeudTemp, pUtilisateur);
                noeudUtilisateur = noeudTemp.Previous;
            }
        }

        /// <summary>
        /// Met à jour le dictionnaire de statistiques du joueur humain
        /// </summary>
        /// <param name="pUtilisateur">Le joueur dont les statistiques doivent être mise à jour</param>
        public static void MettreAJourStats(Utilisateur pUtilisateur)
        {
            dictUtilisateurs[pUtilisateur.Nom] = pUtilisateur; 
        }
    }
}
