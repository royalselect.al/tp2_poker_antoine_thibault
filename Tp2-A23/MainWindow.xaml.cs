﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tp2_A23.classes;

namespace Tp2_A23
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Partie laPartie;
        private Utilisateur utilisateur; 
        private bool attenteActionHumain = false;

        public MainWindow()
        {
            InitializeComponent();
            btnJouerUneMain.IsEnabled = false;
            btnStatistiques.IsEnabled = false;
            Donnees.ChargerDonnees();
        }

        #region "Boutons à gauche"
        private void btnAuthentification_Click(object sender, RoutedEventArgs e)
        {
            Authentification fenAuthentification = new Authentification();
            fenAuthentification.ShowDialog();
            if (fenAuthentification.utilisateur != null)
            {
                utilisateur = fenAuthentification.utilisateur;
                btnJouerUneMain.IsEnabled = true;
                btnStatistiques.IsEnabled = true;
                laPartie = null;
            }
        }

        private void btnJouerUneMain_Click(object sender, RoutedEventArgs e)
        {
            if (laPartie == null)
                laPartie = new Partie(utilisateur);
                
            laPartie.ViderTable();
            lbLog.Items.Add("Nouvelle main");
            laPartie.Joueurs.Peek().Utilisateur.MainJouees.Add(new mainJouee());
            laPartie.CommencerUneMain();
            AfficherCartesJoueur();
            AfficherCartesCommunes();
            AfficherDonneur();
            AfficherMisesFonds();
            attenteActionHumain = false;
            btnAuthentification.IsEnabled = false;
            

            JouerProchainJoueur();

        }

        /// <summary>
        /// Cette méthode permet de commencer une nouvelle main
        /// </summary>
        private void NouvelleMain()
        {
            lbLog.Items.Add("Nouvelle main");
            laPartie.CommencerUneMain();
            AfficherCartesJoueur();
            AfficherCartesCommunes();
            AfficherDonneur();
            AfficherMisesFonds();
            attenteActionHumain = false;

            JouerProchainJoueur();
        }

        private void btnClassement_Click(object sender, RoutedEventArgs e)
        {
            Classement fenClassement = new Classement();
            fenClassement.ShowDialog();
        }

        private void btnStatistiques_Click(object sender, RoutedEventArgs e)
        {
            //TODO #6
            Statistiques fenStats = new Statistiques(utilisateur);
            fenStats.ShowDialog();
        }
        #endregion

        #region "Actions du joueur humain"
        private void btnCoucher_Click(object sender, RoutedEventArgs e)
        {
            if (attenteActionHumain)
            {
                lbLog.Items.Add(laPartie.JoueurCourantSeCouche());
                attenteActionHumain = false;
                FinTourJoueur();
            }
               
        }

        private void btnRelancer_Click(object sender, RoutedEventArgs e)
        {
            if (attenteActionHumain)
            {
                lbLog.Items.Add(laPartie.JoueurCourantRelance());
                attenteActionHumain = false;
               
                FinTourJoueur();
            }
        }

        private void btnParler_Click(object sender, RoutedEventArgs e)
        {
            if (attenteActionHumain)
            {
                lbLog.Items.Add(laPartie.JoueurCourantParle());
                attenteActionHumain = false;
                FinTourJoueur();
            }
        }

        private void btnSuivre_Click(object sender, RoutedEventArgs e)
        {
            if (attenteActionHumain)
            {
                lbLog.Items.Add(laPartie.JoueurCourantSuit());
                attenteActionHumain = false;
                FinTourJoueur();
            }
        }
        #endregion

        private void JouerProchainJoueur()
        {
            string action = laPartie.JouerProchainJoueur();
            switch (action)
            {
                case "JoueurCouche":
                    FinTourJoueur();
                    break;
                case "AttenteJoueurHumain":
                    attenteActionHumain = true;
                    break;
                case "FinTour":
                    if (laPartie.CartesCommunes.Count < 5)
                    {
                        lbLog.Items.Add("Ajout d'une carte commune");
                        laPartie.TournantEtRiviere();
                        AfficherCartesCommunes();
                        JouerProchainJoueur();
                    }
                    else
                        FinDeMain();
                    break;
                
                default:
                    lbLog.Items.Add(action);
                    FinTourJoueur();
                    break;
            }
        }

        private void FinDeMain()
        {
            lbLog.Items.Add("Fin de main");
            laPartie.RemettreQueueJoueurEtatInitial();
            AfficherCartesFinMain();
            AfficherMisesFonds();
            lbLog.Items.Add(laPartie.FinDeMain());
            btnAuthentification.IsEnabled = true;
        }

        private void FinTourJoueur()
        {
            AfficherCartesJoueur();
            AfficherMisesFonds();
            JouerProchainJoueur();
        }

        #region "Affichage"
        /// <summary>
        /// Méthode qui permet d'afficher les cartes des joueurs
        /// </summary>
        private void AfficherCartesJoueur()
        {
            int decalageCarte=0;
            cnvJoueur1.Children.Clear();
            //Affichage de la main du joueur humain en face ouverte
            if (laPartie.Joueurs.Peek().EtatJoueur == Joueur.Etat.Couche)
            {
                Image monImage = new Image();
                monImage.Source = BitmapFrame.Create(new Uri("images/fold.png", UriKind.Relative));
                monImage.Width = 96;
                monImage.Height = 150;
                cnvJoueur1.Children.Add(monImage);
            }
            else
            {
                for (int i = 0; i < 2; i++)
                {
                    Image monImage = new Image();
                    monImage.Source =
                        BitmapFrame.Create(new Uri("cartes/" + laPartie.Joueurs.Peek().Main[i].ObtenirNomFichier(),
                            UriKind.Relative));
                    monImage.Width = 72;
                    monImage.Height = 96;
                    Canvas.SetLeft(monImage, decalageCarte);
                    cnvJoueur1.Children.Add(monImage);
                    decalageCarte += 75;
                }
            }
            
            laPartie.Joueurs.Enqueue(laPartie.Joueurs.Dequeue());
            //Affichage de la main des joueurs automatisés en face cachée
            for (int i = 1; i < 4; i++) 
            {
                decalageCarte = 0;
                ((Canvas)maGrid.FindName("cnvJoueur" + (i + 1).ToString())).Children.Clear();
                if (laPartie.Joueurs.Peek().EtatJoueur == Joueur.Etat.Couche)
                {
                    Image monImage = new Image();
                    monImage.Source = BitmapFrame.Create(new Uri("images/fold.png", UriKind.Relative));
                    monImage.Width = 96;
                    monImage.Height = 150;
                    ((Canvas)maGrid.FindName("cnvJoueur" + (i + 1).ToString())).Children.Add(monImage);
                }
                else
                {
                    for (int j = 0; j < 2; j++)
                    {

                        Image monImage = new Image();
                        monImage.Source = BitmapFrame.Create(new Uri("cartes/b2fv.png",UriKind.Relative));
                        monImage.Width = 72;
                        monImage.Height = 96;
                        Canvas.SetLeft(monImage, decalageCarte);
                        ((Canvas)maGrid.FindName("cnvJoueur" + (i + 1).ToString())).Children.Add(monImage);
                        decalageCarte += 75;
                    }
                }
                laPartie.Joueurs.Enqueue(laPartie.Joueurs.Dequeue());
            }
        }

        private void AfficherCartesFinMain()
        {
            int decalageCarte = 0;
            for (int i = 0; i < laPartie.Joueurs.Count; i++)
            {
                decalageCarte = 0;
                ((Canvas)maGrid.FindName("cnvJoueur" + (i + 1).ToString())).Children.Clear();
                if (laPartie.Joueurs.Peek().EtatJoueur == Joueur.Etat.Couche)
                {
                    Image monImage = new Image();
                    monImage.Source = BitmapFrame.Create(new Uri("images/fold.png", UriKind.Relative));
                    monImage.Width = 96;
                    monImage.Height = 150;
                    ((Canvas)maGrid.FindName("cnvJoueur" + (i + 1).ToString())).Children.Add(monImage);
                }
                else
                {
                    for (int j = 0; j < 2; j++)
                    {

                        Image monImage = new Image();
                        monImage.Source = BitmapFrame.Create(new Uri("cartes/" + laPartie.Joueurs.Peek().Main[j].ObtenirNomFichier(),
                            UriKind.Relative));
                        monImage.Width = 72;
                        monImage.Height = 96;
                        Canvas.SetLeft(monImage, decalageCarte);
                        ((Canvas)maGrid.FindName("cnvJoueur" + (i + 1).ToString())).Children.Add(monImage);
                        decalageCarte += 75;
                    }
                }
                laPartie.Joueurs.Enqueue(laPartie.Joueurs.Dequeue());
            }
        }

        /// <summary>
        /// Cette méthode permet d'afficher les cartes communes
        /// </summary>
        private void AfficherCartesCommunes()
        {
            int decalageCarte = 0;
            cnvCartesCommunes.Children.Clear();
            for (int i = 0; i < laPartie.CartesCommunes.Count; i++)
            {
                Image monImage = new Image();
                monImage.Source =
                    BitmapFrame.Create(new Uri("cartes/" + laPartie.CartesCommunes[i].ObtenirNomFichier(),
                        UriKind.Relative));
                monImage.Width = 72;
                monImage.Height = 96;
                Canvas.SetLeft(monImage, decalageCarte);
                cnvCartesCommunes.Children.Add(monImage);
                decalageCarte += 75;
            }
        }

        /// <summary>
        /// Cette méthode permet d'afficher les mises et les fonds
        /// </summary>
        private void AfficherMisesFonds()
        {
            for (int i = 0; i < 4; i++)
            {
                ((TextBox)maGrid.FindName("txtMiseJ" + (i + 1).ToString())).Text = laPartie.Joueurs.Peek().Mise.ToString();
                laPartie.Joueurs.Enqueue(laPartie.Joueurs.Dequeue());
            }
            for (int i = 0; i < 4; i++)
            {
                ((TextBox)maGrid.FindName("txtFondsJ" + (i + 1).ToString())).Text = laPartie.Joueurs.Peek().Fonds.ToString();
                laPartie.Joueurs.Enqueue(laPartie.Joueurs.Dequeue());
            }
        }

        /// <summary>
        /// Cette méthode permet d'afficher le jeton du donneur
        /// </summary>
        private void AfficherDonneur()
        {
            imgDonJ1.Visibility = Visibility.Hidden;
            imgDonJ2.Visibility = Visibility.Hidden;
            imgDonJ3.Visibility = Visibility.Hidden;
            imgDonJ4.Visibility = Visibility.Hidden;
            ((Image)maGrid.FindName("imgDonJ" + (laPartie.IndexDonneur+1).ToString())).Visibility = Visibility.Visible;
        }


        #endregion

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Donnees.SauvegarderDonnees();
        }
    }
}
