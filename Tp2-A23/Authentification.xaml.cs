﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Tp2_A23.classes;

namespace Tp2_A23
{
    /// <summary>
    /// Logique d'interaction pour Authentification.xaml
    /// </summary>
    public partial class Authentification : Window
    {
        public Utilisateur utilisateur { get; set; }

        public Authentification()
        {
            InitializeComponent();
            Donnees.ChargerDonnees();
        }

        private void btnAuthentifier_Click(object sender, RoutedEventArgs e)
        {
            
                if (Donnees.dictUtilisateurs.ContainsKey(txtUtilisateur.Text))
                {
                    if (Login.VerifierMotDePasse(txtMotDePasse.Password,
                        Donnees.dictSalts[txtUtilisateur.Text],
                        Donnees.dictUtilisateurs[txtUtilisateur.Text].Mdp))
                    {
                        MessageBox.Show("Bienvenue");
                        utilisateur = Donnees.dictUtilisateurs[txtUtilisateur.Text];
                    }
                    else
                    {
                        MessageBox.Show("Hum, ce n'est pas ce à quoi nous nous attendions...");
                    }
                }
                else
                {
                    MessageBox.Show("Le compte n'existe pas");
                }
                
            //TODO #3

            Close();
        }

        private void btnCreerCompte_Click(object sender, RoutedEventArgs e)
        {
            //TODO #3
            if (txtUtilisateur.Text.Length > 0 && txtMotDePasse.Password.Length > 0)
            {

                if (Donnees.dictUtilisateurs.ContainsKey(txtUtilisateur.Text))
                {
                    MessageBox.Show("Impossible de créer un compte avec ce nom d'utilisateur");
                }
                else
                {
                    byte[] salage = Login.CreerNouveauSalt();
                    byte[] mdp = Login.HashUnMotDePasse(txtMotDePasse.Password, salage);
                    utilisateur = new Utilisateur(txtUtilisateur.Text, mdp);
                    utilisateur.MainJouees = new List<mainJouee>();

                    MessageBox.Show("Compte créer avec succès !");

                    Donnees.dictUtilisateurs.Add(txtUtilisateur.Text, utilisateur);
                    Donnees.dictSalts.Add(txtUtilisateur.Text, salage);
                    Donnees.classement.AddLast(utilisateur);
                    Donnees.TrierClassement(utilisateur);
                }
                Donnees.SauvegarderDonnees();
                Close();
            }
            else
            {
                MessageBox.Show("Entrez un mot de passe valide");
            }
           
        }
    }
}
